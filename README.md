# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A modification of ROSBridgeLib by Mathias Ciarlo @by https://github.com/MathiasCiarlo/ROSBridgeLib to be used with the Barrett WAM in Unity
* Version 0.1


### How do I get set up? ###

* 1 Put the ROSBridgeLib and WAMSim folders into the Assets of the Unity Project
* 2 Drag the WAM model located in the WAMSim/PreFabs into the scene. You might need to resize the model for your application
* 3 Attach the WAMViewer.cs script under WAMSim/Scripts to the WAM model and just click run
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Currently have most of the wam msg types built, also have the /wam/jnt_pos_cmd and wam/joint_states publisher and subscriber finished.
* WAM Viewer is a simple program that will display the physical wam current position in real time based on the joint_states topic and
* can control the WAM using 7 sliders which publish to each joint using the RTJointPos message.
* added ChannelFloat32 msg, and modified PoseStampedMsg
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Owned by Cole Shing, @coleshing0603@gmail.com
* Other community or team contact
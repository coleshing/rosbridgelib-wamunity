﻿/*  ChannelFloat32 Msg, 
 *  added to ROSBridgeLib by Cole Shing
 *  string name
 *  float32[] values
  */

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using SimpleJSON;
using ROSBridgeLib.std_msgs;

namespace ROSBridgeLib{
    namespace sensor_msgs    {
        public class ChannelFloat32Msg : ROSBridgeMsg{

            private string _name;
            private float[] _values;

            public ChannelFloat32Msg(JSONNode msg)
            {
                _name = msg["name"];
                _values = new float[msg["values"].Count];
                for (int i = 0; i < _values.Length; i++)
                {
                    _values[i] = float.Parse(msg["values"][i]);
                }
            }

            public ChannelFloat32Msg(string name, float[] values)
            {
                _name = name;
                _values = values;
            }

            public static string GetMessageType()
            {
                return "Sensor_msgs/ChannelFloat32";
            }

            public string GetName()
            {
                return _name;
            }

            public float[] GetValues()
            {
                return _values;
            }

            public override string ToString()
            {
                //converting the values array into string
                string valuesarray = "[";
                for (int i = 0; i < _values.Length; i++)
                {
                    valuesarray = valuesarray + _values[i];
                    if (_values.Length - i >= 1 && i < _values.Length - 1)
                        valuesarray += ",";
                }
                valuesarray += "]";

                return "RTJointPos [name=" + _name + ", values= " + valuesarray + "]";
            }

            public override string ToYAMLString()
            {
                //converting the rate_limits array into YAMLstring
                string valuesarray = "[";
                for (int i = 0; i < _values.Length; i++)
                {
                    valuesarray = valuesarray + _values[i];
                    if (_values.Length - i >= 1 && i < _values.Length - 1)
                        valuesarray += ",";
                }
                valuesarray += "]";

                return "{\"name\" : " + _name + ", \"values\" : " + valuesarray + "}";
            }
        }
    }
}


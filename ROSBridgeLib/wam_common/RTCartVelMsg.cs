﻿/*  RTCartVel Msg, publisher to /wam/cart_vel_cmd
 *  added to ROSBridgeLib. To get the messages working for ROS WAM. By Cole Shing
 *  float32[3] direction
 *  float32 magnitude
 */

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using SimpleJSON;
using ROSBridgeLib.std_msgs;

namespace ROSBridgeLib
{
    namespace wam_common
    {
        public class RTCartVelMsg : ROSBridgeMsg
        {
            private float[] _direction;
            private float _magnitude;

            public RTCartVelMsg(JSONNode msg)
            {
                _direction = new float[3];
                for (int i = 0; i < _direction.Length; i++)
                {
                    _direction[i] = float.Parse(msg["direction"][i]);
                }

                _magnitude = float.Parse(msg["magnitude"]);
            }

            public RTCartVelMsg(float[] direction, float magnitude)
            {
                _direction = direction;
                _magnitude = magnitude;
            }

            public static string GetMessageType()
            {
                return "wam_common/RTCartVel";
            }

            public float[] GetDirection()
            {
                return _direction;
            }

            public float GetMagnitude()
            {
                return _magnitude;
            }
            
            public override string ToString()
            {
                //converting the direction array into string
                string directionarray = "[";
                for (int i = 0; i < _direction.Length; i++)
                {
                    directionarray = directionarray + _direction[i];
                    if (_direction.Length - i >= 1 && i < _direction.Length - 1)
                        directionarray += ",";
                }
                directionarray += "]";

                return "RTCartVel [direction=" + directionarray + ", magnitude= " + _magnitude + "]";
            }

            public override string ToYAMLString()
            {
                //converting the direction array into YAMLstring
                string directionarray = "[";
                for (int i = 0; i < _direction.Length; i++)
                {
                    directionarray = directionarray + _direction[i];
                    if (_direction.Length - i >= 1 && i < _direction.Length - 1)
                        directionarray += ",";
                }
                directionarray += "]";

                return "{\"direction\" : " + directionarray + ", \"magnitude\" : " + _magnitude + "}";
            }
        }
    }
}


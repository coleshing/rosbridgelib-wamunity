﻿/*  RTJointVel Msg, publisher to /wam/jnt_vel_cmd
 *  added to ROSBridgeLib. To get the messages working for ROS WAM. By Cole Shing
 *  float32[] velocities
 */

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using SimpleJSON;
using ROSBridgeLib.std_msgs;

namespace ROSBridgeLib
{
    namespace wam_common
    {
        public class RTJointVelMsg : ROSBridgeMsg
        {
            private float[] _velocities;
            
            public RTJointVelMsg(JSONNode msg)
            {
                _velocities= new float[msg["velocities"].Count];
                for (int i = 0; i < _velocities.Length; i++)
                {
                    _velocities[i] = float.Parse(msg["velocities"][i]);
                }
            }

            public RTJointVelMsg(float[] velocities)
            {
                _velocities = velocities;
            }

            public static string GetMessageType()
            {
                return "wam_common/RTJointVel";
            }

            public float[] GetVelocities()
            {
                return _velocities;
            }
            
            public override string ToString()
            {
                //converting the velocities array into string
                string velarray = "[";
                for (int i = 0; i < _velocities.Length; i++)
                {
                    velarray = velarray + _velocities[i];
                    if (_velocities.Length - i >= 1 && i < _velocities.Length - 1)
                        velarray += ",";
                }
                velarray += "]";

                return "RTJointVel [velocities=" + velarray + "]";
            }

            public override string ToYAMLString()
            {
                //converting the velocities array into YAMLstring
                string velarray = "[";
                for (int i = 0; i < _velocities.Length; i++)
                {
                    velarray = velarray + _velocities[i];
                    if (_velocities.Length - i >= 1 && i < _velocities.Length - 1)
                        velarray += ",";
                }
                velarray += "]";

                return "{\"velocities\" : " + velarray + "}";
            }
        }
    }
}


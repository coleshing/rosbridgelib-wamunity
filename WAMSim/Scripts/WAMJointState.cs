﻿using ROSBridgeLib;
using ROSBridgeLib.wam_common;
using System.Collections;
using System.Text;
using SimpleJSON;
using UnityEngine;

/* 
 * Joint state subscriber, subscribes to wam/joint_states through the JointStateMsg
 * written by Cole Shing
 */
public class WAMJointState : ROSBridgeSubscriber {
       
    public new static string GetMessageTopic()
    {
        return "/wam/joint_states";
    }

    public new static string GetMessageType()
    {
        return "sensor_msgs/JointState";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new ROSBridgeLib.sensor_msgs.JointStateMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        float[] position = new float[7]; // the postions in rads of the WAM in float
        double[] positiond = new double[7]; //the positions in rads of the WAM in double
        float[] angle = new float[7]; //the position in degrees of the WAM
        GameObject robot = GameObject.Find("WAM");
        if (robot == null)
            Debug.Log("Can't find the robot???");
        else
        {
            GameObject[] Rotations= new GameObject[7]; //finding all 7 rotations
            Rotations[0] = GameObject.Find("B2124"); //J1 horizontal base rotation
            Rotations[1] = GameObject.Find("B2125"); //J2 vertical base rotation
            Rotations[2] = GameObject.Find("B2126"); //J3 upper arm rotation around axis
            Rotations[3] = GameObject.Find("B2127"); //J4 eblow joint
            Rotations[4] = GameObject.Find("B3308"); //J5 rotation of the wrist
            Rotations[5] = GameObject.Find("B2573"); //J6 Flex of the wrist
            Rotations[6] = GameObject.Find("B2574"); //J7 rotation of the tool plate
            
            ROSBridgeLib.sensor_msgs.JointStateMsg jointstate = (ROSBridgeLib.sensor_msgs.JointStateMsg)msg;
            positiond = jointstate.GetPosition(); //get the positions from the message
            for (int i = 0; i < positiond.Length; i++) {
                position[i] = System.Convert.ToSingle(positiond[i]);
                angle[i]  = Mathf.Rad2Deg * position[i]; //convert to degrees
            }
            //setting rotations to the correct angle
            Rotations[0].transform.localRotation = Quaternion.Euler(0, -angle[0]-90, 0); //J1 rotates around the base, negative due to physical rotation
            Rotations[1].transform.localRotation = Quaternion.Euler(angle[1], 90, 0); //J2 rotates vertically
            Rotations[2].transform.localRotation = Quaternion.Euler(0, -angle[2] - 180, 0); //J3 rotates around the axis of the arm
            Rotations[3].transform.localRotation = Quaternion.Euler(angle[3] -90 , 180, 0); //J4 bends the elbow
            Rotations[4].transform.localRotation = Quaternion.Euler(0, -angle[4], 0); //J5 rotates around the wrist, negative due to physical rotation
            Rotations[5].transform.localRotation = Quaternion.Euler(angle[5], 0, 0); //J6 bends the wrist
            Rotations[6].transform.localRotation = Quaternion.Euler(0, angle[6], 0); //J6 rotates the wrist
        }
        Debug.Log("Render callback in /wam/joint_states" + msg);
        Debug.Log("position Joint1 " + position[0]);
    }
}

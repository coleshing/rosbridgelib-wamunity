﻿using ROSBridgeLib;
using ROSBridgeLib.wam_common;
using System.Collections;
using System.Text;
using SimpleJSON;
using UnityEngine;

/* cart_pos_cmd publisher using RTCartPosMsg to publish to /wam/cart_pos_cmd
 * written by Cole Shing.
 */
public class WAMRTCartPos : ROSBridgePublisher
{

    public static string GetMessageTopic()
    {
        return "/wam/cart_pos_cmd";
    }

    public static string GetMessageType()
    {
        return "wam_common/RTCartPos";
    }

    public static string ToYAMLString(RTCartPosMsg msg)
    {
        return msg.ToYAMLString();
    }
}

﻿using ROSBridgeLib;
using ROSBridgeLib.wam_common;
using System.Collections;
using System.Text;
using SimpleJSON;
using UnityEngine;

/* cart_vel_cmd publisher using RTCartVelMsg to publish to /wam/cart_vel_cmd
 * written by Cole Shing.
 */
public class WAMRTCartVel : ROSBridgePublisher
{

    public static string GetMessageTopic()
    {
        return "/wam/cart_vel_cmd";
    }

    public static string GetMessageType()
    {
        return "wam_common/RTCartVel";
    }

    public static string ToYAMLString(RTCartVelMsg msg)
    {
        return msg.ToYAMLString();
    }
}

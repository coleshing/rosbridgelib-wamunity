﻿using ROSBridgeLib;
using ROSBridgeLib.wam_common;
using System.Collections;
using System.Text;
using SimpleJSON;
using UnityEngine;

/* jnt_pos_cmd publisher using RTJointPosMsg to publish to /wam/jnt_pos_cmd
 * written by Cole Shing.
 */
public class WAMRTJointPos : ROSBridgePublisher
{

    public static string GetMessageTopic()
    {
        return "/wam/jnt_pos_cmd";
    }

    public static string GetMessageType()
    {
        return "wam_common/RTJointPos";
    }

    public static string ToYAMLString(RTJointPosMsg msg)
    {
        return msg.ToYAMLString();
    }
}

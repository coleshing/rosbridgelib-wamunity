﻿using ROSBridgeLib;
using ROSBridgeLib.wam_common;
using System.Collections;
using System.Text;
using SimpleJSON;
using UnityEngine;

/* jnt_vel_cmd publisher using RTJointVelMsg to publish to /wam/jnt_vel_cmd
 * written by Cole Shing.
 */
public class WAMRTJointVel : ROSBridgePublisher
{

    public static string GetMessageTopic()
    {
        return "/wam/jnt_vel_cmd";
    }

    public static string GetMessageType()
    {
        return "wam_common/RTJointVel";
    }

    public static string ToYAMLString(RTJointVelMsg msg)
    {
        return msg.ToYAMLString();
    }
}

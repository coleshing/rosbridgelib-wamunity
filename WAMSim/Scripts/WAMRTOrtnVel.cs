﻿using ROSBridgeLib;
using ROSBridgeLib.wam_common;
using System.Collections;
using System.Text;
using SimpleJSON;
using UnityEngine;

/* ortn_vel_cmd publisher using RTOrtnVelMsg to publish to /wam/ortn_vel_cmd
 * written by Cole Shing.
 */
public class WAMRTOrtnVel : ROSBridgePublisher
{

    public static string GetMessageTopic()
    {
        return "/wam/ortn_vel_cmd";
    }

    public static string GetMessageType()
    {
        return "wam_common/RTOrtnVel";
    }

    public static string ToYAMLString(RTOrtnVelMsg msg)
    {
        return msg.ToYAMLString();
    }
}

﻿using UnityEngine;
using ROSBridgeLib;
using ROSBridgeLib.wam_common;
using System; //for the boolean

/*
 * Written by Cole Shing
 */
// J1 +/-2.6(150), J2 +/-2.0(113), J3 +/-2.8(157), 
// J4 +3.1/-0.9 (180/-50), J5 +1.24/-4.76(71/-273), 
// J6 +/-1.6(90), J7 +/-3.0(172)

/* position is in meter units, x positive is point away
 * from the cable side, y is perpendicular on a flat plane
 * z positive is the vertical height of the end effector.
 */
public class WAMViewer : MonoBehaviour {

    public Boolean Use_Sliders;
    float[] sliders = { 0.0F, -2.0F, 0.0F, 3.1F, 0.0F, 0F, 0.0F }; //starting slider positions
    float[] currentAngles = { 0, -113, 0, 180, 0, -90, 0 }; //starting angles of the robot
    private ROSBridgeWebSocketConnection ros = null; //defined in ROSBridgeWebSocketConnection
    private Boolean _useJoysticks; //using joystick or not check

    // Define our subscribers, publishers and service response handlers
    void Start()
    {
        FloorTile.Floor(0, 0, 12, 12); //defined in FloorTile script
        _useJoysticks = Input.GetJoystickNames().Length > 0; //find if there are joysticks

        //creates the connection to the bridge
        ros = new ROSBridgeWebSocketConnection("ws://137.82.173.72", 9090); //change to IP of ROS machine
        ros.AddSubscriber(typeof(WAMJointState));
        ros.AddSubscriber(typeof(WAMPose));
        ros.AddPublisher(typeof(WAMRTJointPos)); //used with sliders
        ros.AddPublisher(typeof(WAMRTCartPos)); //used to control the WAM with the point
        ros.Connect(); //actually connects to the ros bridge
    }

    // When application close, disconnect to ROS bridge
    void OnApplicationQuit()
    {
        if (ros != null)
            ros.Disconnect(); //extremely important to disconnect from ROS.OTherwise packets continue to flow
    }

    // Update is called once per frame in Unity. The Unity camera follows the robot (which is driven by
    // the ROS environment. We also use the joystick or cursor keys to generate teleoperational commands
    // that are sent to the ROS world, which drives the robot which ...
    void Update()
    {
        float _dx, _dy;  // x and y direction value
        int _button = 0; //button for ?


        if (_useJoysticks)  //assign x and y to joystick axis if there is
        {
            _dx = Input.GetAxis("Joy0X");
            _dy = Input.GetAxis("Joy0Y");
        }
        else //otherwise just use whatever input is horizontal and vertical set in unity input 
        {
            _dx = Input.GetAxis("Horizontal");
            _dy = Input.GetAxis("Vertical");
            //Debug.Log ("no joysticks " + _dx + " " + _dy);
        }
        float linear = _dy * 0.5f; //vertical is set to linear movement
        float angular = -_dx * 0.2f; //horizontal is set to angular rotation

        if (Use_Sliders == true)
        {
            float[] joints = new float[7];
            float[] rate_limits = new float[7];

            for (int i = 0; i < 7; i++)
            {
                joints[i] = sliders[i];
                rate_limits[i] = System.Convert.ToSingle(0.1);
            }

            ////creates the msg to publish to send control the WAM
            RTJointPosMsg msg = new RTJointPosMsg(joints, rate_limits);
            ros.Publish(WAMRTJointPos.GetMessageTopic(), msg);
        }
        ros.Render(); //pretty much same as ros.spin()
    }

    void OnGUI()
    {
        sliders[0] = GUI.HorizontalSlider(new Rect(25, 25, 100, 30), sliders[0], -2.6F, 2.6F);
        sliders[1] = GUI.HorizontalSlider(new Rect(25, 55, 100, 30), sliders[1], -2.0F, 2.0F);
        sliders[2] = GUI.HorizontalSlider(new Rect(25, 85, 100, 30), sliders[2], -2.8F, 2.8F);
        sliders[3] = GUI.HorizontalSlider(new Rect(25, 115, 100, 30), sliders[3], -0.9F, 3.1F);
        sliders[4] = GUI.HorizontalSlider(new Rect(25, 145, 100, 30), sliders[4], -4.76F, 1.24F);
        sliders[5] = GUI.HorizontalSlider(new Rect(25, 175, 100, 30), sliders[5], -1.6F, 1.6F);
        sliders[6] = GUI.HorizontalSlider(new Rect(25, 205, 100, 30), sliders[6], -3.0F, 3.0F);
    }
}